import math

from flask import (
    abort, Blueprint, request, render_template
)

from pmenu.routes.lib import init, give_levels
from pmenu.menu import get_all_menus
from pmenu.pagination import pagination
from pmenu.conf import Conf


menu_page = Blueprint(
    'menu_page',
    __name__,
    template_folder='templates'
)


@menu_page.route('/menu', methods=['GET'])
async def menu():
    preferences = init.db_prefs.get()
    filter_by = 0
    if 'filter_by' in request.args:
        filter_by = int(request.args['filter_by'])

    session_name = request.args['session_name']
    # Impossible d'être sur une session non existante
    if session_name not in Conf.sessions:
        abort(404)

    session_selected = session_name
    if 'session_selected' in request.args:
        session_selected = request.args['session_selected']

    # Impossible de sélectionner une session non existante
    if session_selected not in Conf.sessions:
        abort(404)

    session = init.sessions[session_name]

    try:
        levels = get_all_menus(init.db.get_levels(
            session_selected,
            filter_by
        ).fetchall())[session_selected]['menu']
    except KeyError:
        abort(404)

    page_index = 0
    level_name, sub_level_name = give_levels(request, levels)

    if session_name == 'poe':
        nb_apps_for_session = init.db.count_apps_from_level(
            session_selected,
            filter_by
        )
    if session_name == 'poe' and level_name is None and sub_level_name is None:
        nb_apps = nb_apps_for_session
    else:
        nb_apps = init.db.count_apps_from_level(
            session_selected,
            filter_by,
            level_name,
            sub_level_name
        )
    results_by_page = init.conf.results_by_page
    nb_pages = math.ceil(nb_apps / init.conf.results_by_page)

    init.db_prefs.update({
        'session_selected': session_selected,
        'filter_by': filter_by
    })
    apps = init.db.get_page_from_level(
        0,
        results_by_page,
        session_name,
        session_selected,
        filter_by,
        level_name,
        sub_level_name
    ).fetchall()
    apps = render_template(
        'apps.html',
        session=session,
        session_name=session_name,
        preferences=preferences,
        apps=apps,
        nb_apps=nb_apps,
        nb_pages=nb_pages
    )
    menu = render_template(
        'horizontal_categories_menu.html',
        session=session,
        session_name=session_name,
        preferences={"filter_by": filter_by},
        menu=levels,
        level_name=level_name,
        sub_level_name=sub_level_name,
        nb_apps=nb_apps,
        nb_apps_for_session=nb_apps_for_session
    )
    pagination_t = render_template(
        'pagination.html',
        page_index=0,
        session=session,
        session_name=session_name,
        level_name=level_name,
        sub_level_name=sub_level_name,
        pagination=pagination(page_index, nb_pages)
    )
    return {
        'apps': apps,
        'menu': menu,
        'pagination': pagination_t
    }
